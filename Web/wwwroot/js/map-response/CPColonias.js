﻿function direccion(cp) {
    var $Ciudad = $("#Colonia");
    var valuepre = "";
    try {
        valuepre = document.getElementById("ColoniaPre").value;
    } catch (e) {
        valuepre = "";
    }

    console.log(valuepre);
    url = 'https://direcciones.ozelot.it/api/DireccionAPI/' + cp;

    $.getJSON(url, null, function (response) {
        $Ciudad.find('option')
            .remove()
            .end()
            .append('<option value="">Selecciona una colonia</option>');
        console.log(response);
        for (var i = 0; i < response.colonias.length; i++) {
            if (valuepre == response.colonias[i]) {
                $Ciudad.append("<option selected value='" + response.colonias[i] + "'>" + response.colonias[i] + "</option>");
            }
            else {
                $Ciudad.append("<option value='" + response.colonias[i] + "'>" + response.colonias[i] + "</option>");
            }

        }
    });
}

var cp = document.getElementById("CP").value;
if (cp != "") {
    direccion(cp);

}


$("#CP").change(function () {
    datos = {};
    var cp = document.getElementById("CP").value;
    if (cp != "") {
        direccion(cp);
    }
});
