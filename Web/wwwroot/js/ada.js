var ada = [

    { centimetros: 147, PesoMinimo: 54, PesoMaximo: 64, valor: 1 },
    { centimetros: 147, PesoMinimo: 65, PesoMaximo: 86, valor: 2 },
    { centimetros: 147, PesoMinimo: 87, PesoMaximo: "+", valor: 3 },

    { centimetros: 150, PesoMinimo: 56, PesoMaximo: 67, valor: 1 },
    { centimetros: 150, PesoMinimo: 68, PesoMaximo: 89, valor: 2 },
    { centimetros: 150, PesoMinimo: 90, PesoMaximo: "+", valor: 3 },

    { centimetros: 152, PesoMinimo: 58, PesoMaximo: 69, valor: 1 },
    { centimetros: 152, PesoMinimo: 70, PesoMaximo: 92, valor: 2 },
    { centimetros: 152, PesoMinimo: 93, PesoMaximo: "+", valor: 3 },

    { centimetros: 155, PesoMinimo: 60, PesoMaximo: 71, valor: 1 },
    { centimetros: 155, PesoMinimo: 72, PesoMaximo: 95, valor: 2 },
    { centimetros: 155, PesoMinimo: 96, PesoMaximo: "+", valor: 3 },

    { centimetros: 157, PesoMinimo: 62, PesoMaximo: 74, valor: 1 },
    { centimetros: 157, PesoMinimo: 74, PesoMaximo: 98, valor: 2 },
    { centimetros: 157, PesoMinimo: 99, PesoMaximo: "+", valor: 3 },

    { centimetros: 160, PesoMinimo: 64, PesoMaximo: 76, valor: 1 },
    { centimetros: 160, PesoMinimo: 77, PesoMaximo: 101, valor: 2 },
    { centimetros: 160, PesoMinimo: 102, PesoMaximo: "+", valor: 3 },

    { centimetros: 163, PesoMinimo: 66, PesoMaximo: 78, valor: 1 },
    { centimetros: 163, PesoMinimo: 79, PesoMaximo: 105, valor: 2 },
    { centimetros: 163, PesoMinimo: 106, PesoMaximo: "+", valor: 3 },

    { centimetros: 165, PesoMinimo: 68, PesoMaximo: 81, valor: 1 },
    { centimetros: 165, PesoMinimo: 82, PesoMaximo: 108, valor: 2 },
    { centimetros: 165, PesoMinimo: 109, PesoMaximo: "+", valor: 3 },

    { centimetros: 168, PesoMinimo: 70, PesoMaximo: 84, valor: 1 },
    { centimetros: 168, PesoMinimo: 85, PesoMaximo: 112, valor: 2 },
    { centimetros: 168, PesoMinimo: 113, PesoMaximo: "+", valor: 3 },

    { centimetros: 170, PesoMinimo: 72, PesoMaximo: 86, valor: 1 },
    { centimetros: 170, PesoMinimo: 87, PesoMaximo: 115, valor: 2 },
    { centimetros: 170, PesoMinimo: 116, PesoMaximo: "+", valor: 3 },

    { centimetros: 173, PesoMinimo: 74, PesoMaximo: 89, valor: 1 },
    { centimetros: 173, PesoMinimo: 90, PesoMaximo: 118, valor: 2 },
    { centimetros: 173, PesoMinimo: 119, PesoMaximo: "+", valor: 3 },

    { centimetros: 175, PesoMinimo: 77, PesoMaximo: 92, valor: 1 },
    { centimetros: 175, PesoMinimo: 93, PesoMaximo: 122, valor: 2 },
    { centimetros: 175, PesoMinimo: 123, PesoMaximo: "+", valor: 3 },

    { centimetros: 178, PesoMinimo: 79, PesoMaximo: 94, valor: 1 },
    { centimetros: 178, PesoMinimo: 95, PesoMaximo: 126, valor: 2 },
    { centimetros: 178, PesoMinimo: 127, PesoMaximo: "+", valor: 3 },

    { centimetros: 180, PesoMinimo: 81, PesoMaximo: 97, valor: 1 },
    { centimetros: 180, PesoMinimo: 98, PesoMaximo: 129, valor: 2 },
    { centimetros: 180, PesoMinimo: 130, PesoMaximo: "+", valor: 3 },

    { centimetros: 183, PesoMinimo: 83, PesoMaximo: 100, valor: 1 },
    { centimetros: 183, PesoMinimo: 101, PesoMaximo: 133, valor: 2 },
    { centimetros: 183, PesoMinimo: 134, PesoMaximo: "+", valor: 3 },

    { centimetros: 185, PesoMinimo: 86, PesoMaximo: 103, valor: 1 },
    { centimetros: 185, PesoMinimo: 104, PesoMaximo: 137, valor: 2 },
    { centimetros: 185, PesoMinimo: 138, PesoMaximo: "+", valor: 3 },

    { centimetros: 188, PesoMinimo: 88, PesoMaximo: 105, valor: 1 },
    { centimetros: 188, PesoMinimo: 106, PesoMaximo: 141, valor: 2 },
    { centimetros: 188, PesoMinimo: 142, PesoMaximo: "+", valor: 3 },

    { centimetros: 191, PesoMinimo: 91, PesoMaximo: 108, valor: 1 },
    { centimetros: 191, PesoMinimo: 109, PesoMaximo: 144, valor: 2 },
    { centimetros: 191, PesoMinimo: 145, PesoMaximo: "+", valor: 3 },

    { centimetros: 193, PesoMinimo: 92, PesoMaximo: 111, valor: 1 },
    { centimetros: 193, PesoMinimo: 112, PesoMaximo: 148, valor: 2 },
    { centimetros: 193, PesoMinimo: 149, PesoMaximo: "+", valor: 3 }
];

function optenerResultadosCentimetro(centimetros) {
    var resultado = ada.filter(imc => imc.centimetros === centimetros);
    return resultado;
}

//function optenerResultadoAda(centimetros, pesoMinimo) {
//    var filtro = ada.filter(imc => imc.centimetros === centimetros);
//    var resultado = filtro.find(peso1 => peso1.PesoMinimo === pesoMinimo);
//    return resultado.valor;
//}


console.log(optenerResultadosCentimetro(144));
console.log(optenerResultadoAda(144, 53)); 
