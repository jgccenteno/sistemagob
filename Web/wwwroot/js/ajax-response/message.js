﻿function onSuccess(result) {
    if (result.success) {
        swal({
            title: "¡Correcto!",
            text: result.msj,
            type: "success"
        }).then(function () {
            location.reload();

        });
    } else {
        swal({
            title: "¡Error!",
            text: result.msj,
            type: "error"
        }).then(function () {

        });
    }
};