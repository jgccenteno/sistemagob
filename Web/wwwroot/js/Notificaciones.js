﻿function refresh(value) {
    $("#noti").load(location.href + " #noti-area");

    $.ajax({
        url: '/Notificaciones/ContarAvisosNV?id=' + value,
        method: "GET",
        contentType: 'json',
        success: function (result) {

            if (result.success) {

                var num = result.count;
                $("#count-uno").remove();
                $("#count-dos").remove();
                $("#noti-last-update").remove();
                //var fecha = $.datepicker.formatDate('dd/mm/yy', new Date());



                $("#activity").append('<b id="count-uno" class="badge">' + num + '</b>');
                $("#noti-count").append('<label id="count-dos"><b>Bandeja de entrada (' + num + ')</b></label>');
                //$("#noti-time-update").append('<b id="noti-last-update">Última actualización: <Label>' + fecha + '</Label></b>');
            } else {

                $("#noti-area").addClass("hidden");
                $("#noti-area-error").removeClass("hidden");
                $("#noti").load(location.href + " #noti-area-error");
            }

        }
    });





}

function AbrirBandeja(url) {
    //alert(url);
    window.location.href = url;

}

function EstatusVisto(value, url) {
    $.ajax({
        url: '/Notificaciones/EditarEstatusVisto?id=' + value,
        method: "GET",
        contentType: 'json',
        success: function (result) {

            if (result.success) {
                window.location.href = url;
            } else {
                swal({
                    type: 'error',
                    title: '¡Error!',
                    text: "Ocurrio un error en el servicio de notificaciones, comunicate con el administrador.",
                    allowOutsideClick: false,
                    allowEscapeKey: false
                }).then(function () {
                    window.location.href = '/Home/Index';
                });
            }

        }
    });
}