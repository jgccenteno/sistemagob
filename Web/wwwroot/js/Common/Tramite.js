﻿
function AgregarURLJS(conURL) {

    if (conURL <= 2) {
        var url = '<div id="urls-' + conURL + '" class="box-limited"><div class="row"><div class="col-md-12"><div class="form-group"><label>URL</label><input type="text" class="form-control" name="urls[' + conURL + ']" value="" placeholder="https://ejemplo.com" /></div></div></div></div>';
        $("#url-container").append(url);
        conURL++;
    }
    return conURL;
}
function Eliminar(conURL) {
    if (conURL > 1) {
        $("#urls-" + (conURL - 1)).remove();
        conURL--;
    }
    return conURL;
}


function AgregarRequisito(cont) {

    if (cont <= 10) {
        var requistiro = '<div class="row" id="requisitos-' + cont + '"><legend></legend><div class="col-md-12"><div class="row"><div class="col-md-6"><div class="form-group"><label> Nombre</label><input name="requisitosList[' + cont + '].Nombre" class="form-control" placeholder="Nombre" type="text" data-val="true" data-val-required="El Nombre es requerido"><span class="text-danger field-validation-valid" data-valmsg-for="requisitosList[' + cont + '].Nombre" data-valmsg-replace="true"></span ></div></div><div class="col-md-3"><div class="form-group"><label>Original</label><select name="requisitosList[' + cont + '].Original" class="form-control" style="height: auto;"><option value="">Seleccione una opción</option><option value="SI">SI</option><option value="NO">NO</option></select></div></div> <div class="col-md-3"><div class="form-group"><label>Número de copias</label><input name="requisitosList[' + cont + '].Copias" class="form-control" placeholder="Número de copias" type="number" onkeypress="return validar($( this ).val())" min="0"></div></div><div class="col-md-12"><div class="col-md-12"><div class="form-group"><label>Observaciones:</label><textarea name="requisitosList[' + cont + '].Observaciones" class="form-control" placeholder="Observaciones"></textarea></div></div></div></div></div></div>';
        $("#requisitos-container").append(requistiro);
        cont++;
    }
    return cont;
}

function EliminarRequisitoJS(cont) {
    if (cont > 1) {
        $("#requisitos-" + (cont - 1)).remove();
        cont--;
    }
    return cont;
}

function EnviaEstado(id, opcion) {
    if (opcion === '1') {
        console.log("REVISION");
        document.getElementById("estado").value = 1;
    } else if (opcion === '2') {
        console.log("EDICION");
        document.getElementById("estado").value = 0;
    } else if (opcion === '3') {
        console.log("REVISION_PUBLICACION");
        document.getElementById("estado").value = 2;
    } else if (opcion === '4') {
        console.log("PUBLICADO");
        document.getElementById("estado").value = 3;
    }
    console.log(document.getElementById("estado").value);
}

