﻿var btnActivo = $("li.active");
var tabActivo = btnActivo.val();
console.log("entro");

$(document).ready(function () {
    $("div.contenedor-body").attr("class", "");
    btnActivo = $("li.active");
    tabActivo = btnActivo.val();
});


$('#prev').on('click', function () {
    if (!$(this).hasClass('disabled')) {
        tabActivo = btnActivo.val();
        DesactivarBtn(tabActivo);
        tabActivo -= 1;
        ActivarBtn(tabActivo);
        ActDesPrevNext(tabActivo);
    }
});

$('#nxt').on('click', function () {
    if (!$(this).hasClass('disabled')) {
        tabActivo = btnActivo.val();
        DesactivarBtn(tabActivo);
        tabActivo += 1;
        ActivarBtn(tabActivo);
        ActDesPrevNext(tabActivo);
    }
});

$("li.paginador").on('click', function () {
    console.log("entro numero");
    if (!$(this).hasClass('disabled')) {
    tabActivo = btnActivo.val();
    DesactivarBtn(tabActivo);
    btnActivo = $(this);
    tabActivo = btnActivo.val();
    ActivarBtn(tabActivo);
    ActDesPrevNext(tabActivo);
    }
});

function ActivarBtn(n) {
    btnActivo = $('#paginador-' + n);
    btnActivo.addClass('active');
    btnActivo.addClass('disabled');
    $('#tab-' + n).removeClass('invisible');
    console.log('#tab-' + n);
}

function DesactivarBtn(n) {
    var btn = $('#paginador-' + n);
    btn.removeClass('disabled');
    btn.removeClass('active');
    $('#tab-' + n).addClass('invisible');
    console.log('#tab-' + n);

}

function ActDesPrevNext(n) {
    var max = $('.paginador').length;
    if (n == 1) {
        $('#nxt').removeClass('disabled');
        $('#prev').addClass('disabled');
    } else if (n == max) {
        $('#prev').removeClass('disabled');
        $('#nxt').addClass('disabled');
    } else {
        $('#prev').removeClass('disabled');
        $('#nxt').removeClass('disabled');
    }
}