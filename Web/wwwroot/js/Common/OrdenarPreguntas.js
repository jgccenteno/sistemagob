﻿
// activate Nestable for list 1
$('#preguntasSinOrdenar').nestable({
    group: 1,
    maxDepth:1
});

// activate Nestable for list 2
$('#PreguntasOrdenadas').nestable({
    group: 1,
    maxDepth: 1
}).on('change', verificaVacio);

var verificaVacio = function () {
    var Cont = $("#PreguntasOrdenadas li").length;
    var li = $("#vacio");
    if (Cont>1){
        li.addClass("hidden");
    } else {
        li.removeClass("hidden");
    }
}

function GuadarOrden() {
    var Cont = $("#PreguntasOrdenadas li").length;
    if (Cont > 0) {
        var list = $("#PreguntasOrdenadas");
        var json = JSON.stringify(list.nestable('serialize'));
        $.ajax({
            url: '/Cuestionarios/OrdenarPreguntas?json=' + json,
            method: "POST",
            success: function (result) {
                if (result.success) {
                    swal({
                        title: "¡Correcto!",
                        text: result.msj,
                        type: "success",
                        allowOutsideClick: false,
                        confirmButtonColor: '#0c7cd5',
                        allowEscapeKey: false
                    }).then(function () {
                            location.href = "/Cuestionarios/";
                        });
                } else {
                    swal({
                        title: "¡Error!",
                        text: result.msj,
                        type: "error",
                        allowOutsideClick: false,
                        confirmButtonColor: '#0c7cd5',
                        allowEscapeKey: false
                    });
                }
            },
            error: function () {
                swal({
                    title: "¡Algo salió mal!",
                    text: "¡Ocurrió un error al realizar la acción, si el problema continua comunicate con el administrador!",
                    type: "error",
                    allowOutsideClick: false,
                    confirmButtonColor: '#0c7cd5',
                    allowEscapeKey: false
                });
            }
        });
    }
    return false;
}