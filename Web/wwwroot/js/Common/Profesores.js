﻿$(document).ready(function () {
    pagefunction();
    $('#select-grupos').select2({
        tags: true,
        placeholder: "SELECCIONAR....",
        allowClear: true,
        selectOnClose: false,
        language: {
            noResults: function (params) {
                return "SELECCIONE UNA INSTITUCIÓN..."
            }
        }
    });

    //$("#FechaNacimiento").datepicker({
    //    autoclose: true,
    //    startView: 2
    //});

});

$("#select-institucion").on('change', function () {
    $("#grupos-msj-2").addClass("invisible");
    $("#grupos-msj-1").removeClass("invisible");

    if ($("#select-institucion").val() != "") {
        $('#select-grupos')
            .find('option')
            .remove()
            .end()
        var idInstituto = $("#select-institucion").val();
        url = '/Profesores/ObtenerGruposPorInstitucion?id=' + idInstituto;
        datos = {};
        FillGruposCombo(url);
    }
});

function FillGruposCombo(url) {

    $.ajax({
        url: url,
        method: "GET",
        dataType: 'json',
        contentType: 'application/json',
        processData: false,
        success: function (data) {
            console.log(data);
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    //console.log(data[i].id + " - " + data[i].nombre)
                    $("#select-grupos").append("<option value=" + data[i].id + ">" + data[i].nombre + "</option>");
                }
                //$('#select-grupos').select2({
                //    placeholder: "SELECCIONE GRUPOS"
                //});
                $("#grupos-msj-1").addClass("invisible");
                $("#grupos-msj-2").removeClass("invisible");
               
            } else {
                swal({
                    title: "Importante",
                    text: "La institución seleccionada no cuenta con Grupos",
                    type: "warning",
                    allowOutsideClick: false,
                    confirmButtonColor: '#0c7cd5',
                    allowEscapeKey: false
                });
            }
        },
        error: function () {
            swal({
                title: "¡Algo salió mal!",
                text: "Error al cargar los Grupos, comuniquese con el administrador",
                type: "error",
                allowOutsideClick: false,
                confirmButtonColor: '#0c7cd5',
                allowEscapeKey: false
            });
        }
    });
}