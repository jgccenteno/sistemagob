﻿function CrearReporte() {
    var formData = new FormData();
    var totalFiles = document.getElementById("archivos").files.length;
    var descripcion = $('#DescripcionReporte').val();
    var IdEvidenciaRequisito = $('#IdEvidenciaRequisito').val();
    var IdExpediente = $('#IdExpediente').val();

    if (totalFiles > 0) {
        for (var i = 0; i < totalFiles; i++) {
            var file = document.getElementById("archivos").files[i];
            formData.append("archivos", file);
        }
    }
    if (descripcion !== null && descripcion !== "") {
        formData.append("DescripcionReporte", descripcion);
        console.log("Este es el resutado:" + formData);

        swal({
            title: '¿Está seguro que desea crear el reporte para este requisito?',
            text: 'El reporte será enviado al propietario para su subsanación',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#43A047',
            cancelButtonColor: '#0c7cd5',
            confirmButtonText: '¡Sí, crear!',
            cancelButtonText: '¡No, cancelar!',
            confirmButtonClass: 'btn btn-raised btn-danger',
            cancelButtonClass: 'btn btn-raised btn-primary',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function () {
                    $.ajax({
                        url: "/Expedientes/CrearReporte/?id=" + IdEvidenciaRequisito,
                        method: "post",
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            if (data.success) {
                                swal({
                                    title: "Correcto",
                                    text: data.message,
                                    type: "success",
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                }).then(function () {
                                    location.href = '/Expedientes/ListaReportes/?id=' + data.id + "&idExpediente=" + IdExpediente;
                                });
                            } else if (!data.success) {
                                swal({
                                    title: "Algo salió mal",
                                    text: data.message,
                                    type: "error",
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                }).then(function () {
                                    location.reload();
                                });
                            }
                        },
                        error: function () {
                            swal({
                                title: "¡Algo salió mal!",
                                text: "¡Verifica que los datos sean correcto e intente nuevamente, si el problema persiste comunícate con el administrador!",
                                type: "error",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }, function () {
                                location.reload();
                            });
                        }
                    });
                });
            },
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    }
    else {
        swal({
            title: "¡Importante!",
            text: "El campo descripción es requerido",
            type: "info",
            allowOutsideClick: false,
            confirmButtonColor: '#0c7cd5',
            allowEscapeKey: false
        }, function () {
            location.reload();
        });

    }


}

function EditarReporte() {
    var formData = new FormData();
    var totalFiles = document.getElementById("archivos").files.length;
    var Id = $('#Id').val();
    var IdExpediente = $('#IdExpediente').val();
      
    var descripcion = $('#DescripcionRespuesta').val();
    console.log(descripcion);
    if (totalFiles > 0) {
        for (var i = 0; i < totalFiles; i++) {
            var file = document.getElementById("archivos").files[i];
            formData.append("archivos", file);
        }
    }
    if (descripcion !== null && descripcion !== "") {
        formData.append("DescripcionRespuesta", descripcion);

        swal({
            title: "¿Estás seguro de guardar los cambios en el reporte?",
            text: "Está acción no se podrá revertir",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#43A047',
            cancelButtonColor: '#0c7cd5',
            confirmButtonText: '¡Sí, guardar!',
            cancelButtonText: '¡No, cancelar!',
            confirmButtonClass: 'btn btn-raised btn-danger',
            cancelButtonClass: 'btn btn-raised btn-primary',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.ajax({
                        url: "/Expedientes/EditarReporte/?id=" + Id,
                        method: "post",
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            console.log(data);
                            if (data.success) {
                                swal({
                                    title: "Correcto",
                                    text: data.message,
                                    type: "success",
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                }).then(function () {
                                    location.href = '/Expedientes/DetalleReporte/?id=' + data.id + "&idExpediente=" + IdExpediente;

                                });
                            } else if (!data.success) {
                                swal({
                                    title: "Algo salió mal",
                                    text: data.message,
                                    type: "error",
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                }).then(function () {
                                    location.reload();
                                });
                            }
                        },
                        error: function () {
                            swal({
                                title: "¡Algo salió mal!",
                                text: "¡Verifica que los datos sean correcto e intente nuevamente, si el problema persiste comunícate con el administrador!",
                                type: "error",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }, function () {
                                location.reload();
                            });
                        }
                    });
                })
            },
            allowOutsideClick: false,
            allowEscapeKey: false
        })
    }
    else {
        swal({
            title: "¡Importante!",
            text: "El campo Repuesta es requerido",
            type: "info",
            allowOutsideClick: false,
            confirmButtonColor: '#0c7cd5',
            allowEscapeKey: false
        }, function () {
            location.reload();
        });

    }


};

function AprobarRequisito(id, nombre) {
    swal({
        title: '¿Está seguro que desea aprobar el documento:' + nombre +' ?',
        text: 'Está acción no se podrá revertir',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#43A047',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Sí, aprobar!',
        cancelButtonText: '¡No, cancelar!',
        confirmButtonClass: 'btn btn-raised btn-danger',
        cancelButtonClass: 'btn btn-raised btn-primary',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function () {
                $.ajax({
                    url: "/Expedientes/AprobarRequisito/?id=" + id,
                    method: "post",
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: "Correcto",
                                text: data.message,
                                type: "success",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        } else if (!data.success) {
                            swal({
                                title: "Algo salió mal",
                                text: data.message,
                                type: "error",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        }
                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Verifica que los datos sean correcto e intente nuevamente, si el problema persiste comunícate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.reload();
                        });
                    }
                });
            });
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function AprobarExpediente(id, nombre) {
    swal({
        title: '¿Está seguro que desea aprobar el expediente: ' + nombre + ' ?',
        text: 'Está acción no se podrá revertir',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#43A047',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Sí, aprobar!',
        cancelButtonText: '¡No, cancelar!',
        confirmButtonClass: 'btn btn-raised btn-danger',
        cancelButtonClass: 'btn btn-raised btn-primary',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function () {
                $.ajax({
                    url: "/Expedientes/AprobarExpediente/?id=" + id,
                    method: "post",
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: "Correcto",
                                text: data.message,
                                type: "success",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        } else if (!data.success) {
                            swal({
                                title: "Algo salió mal",
                                text: data.message,
                                type: "error",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        }
                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Verifica que los datos sean correcto e intente nuevamente, si el problema persiste comunícate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.reload();
                        });
                    }
                });
            });
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function RegresarExpediente(id, nombre) {
    swal({
        title: '¿Está seguro de que desea enviar a corrección el expediente: ' + nombre + ' ?',
        text: 'Está acción no se podrá revertir',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#43A047',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Sí, enviar!',
        cancelButtonText: '¡No, cancelar!',
        confirmButtonClass: 'btn btn-raised btn-danger',
        cancelButtonClass: 'btn btn-raised btn-primary',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function () {
                $.ajax({
                    url: "/Expedientes/RegresarExpediente/?id=" + id,
                    method: "post",
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.success) {
                            swal({
                                title: "Correcto",
                                text: data.message,
                                type: "success",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        } else if (!data.success) {
                            swal({
                                title: "Algo salió mal",
                                text: data.message,
                                type: "error",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        }
                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Verifica que los datos sean correcto e intente nuevamente, si el problema persiste comunícate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.reload();
                        });
                    }
                });
            });
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}
