﻿var cantidad = 0;

function agregarOpcion() {
    //Número Maximo de Opciones
    ConsultarConfiguracion();
    var x = $("#contenedorOpciones .row").length + 1;
    var Cont = x - 1; //para el seguimiento de los campos
    if (x <= cantidad) //max input box allowed
    {
        contenedor = $("#contenedorOpciones");
        //agregar campo
        $(contenedor).append('<div class="row" id="opciones-' + Cont + '"><input name="Opciones[' + Cont + '].Id" class="hidden" type="text" id="Opciones_Id-' + Cont + '" /><div class="col-md-12"><div class="col-md-9"><div class="form-group"><label id="label-' + Cont + '"><font color="red">* </font>Opción ' + x + '</label><input name="Opciones[' + Cont + '].Nombre" id="Opciones_Nombre-' + Cont + '" class="form-control" placeholder="Opción" type="text" data-val="true" data-val-required="El Campo Opción ' + x + ' es requerido"><span class="text-danger field-validation-valid" data-valmsg-for="Opciones[' + Cont + '].Nombre" data-valmsg-replace="true" ></span></div></div><div class="col-md-2"><div class="form-group"><label><font color="red">* </font>Valor</label><input name="Opciones[' + Cont + '].Valor" id="Opciones_Valor-' + Cont + '" type="number" min="0" value="" class="form-control" placeholder="Ejemplo: 0" data-val="true" data-val-required="El Campo Valor es requerido"><span class="text-danger field-validation-valid" data-valmsg-for="Opciones[' + Cont + '].Valor" data-valmsg-replace="true" ></span></div></div><div class="col-md-1"><div class="form-group" style="height:50px;" ><label>&nbsp;</label><button class="btn btn-danger form-control" href="" type="button" rel="tooltip" title="" data-original-title="Eliminar opción" onclick="EliminarOpcion(' + Cont + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button></div></div></div></div>');
    }
    return false;
}

function agregarPonderacion() {
    //Se puede crear n cantidad de configuraciones para la pregunta
    var x = $("#contenedorConfigs .row").length + 1;
    var Cont = x - 1; //para el seguimiento de los campos

    contenedor = $("#contenedorConfigs");
    //agregar campo
    $(contenedor).append('<div class="row" id="config-' + Cont + '"><input name="ConfiguracionesPregunta[' + Cont + '].Id" class="hidden" type="text" id="ConfiguracionesPregunta_Id-' + Cont + '" /><div class="col-md-12"><div class="col-md-3"><div class="form-group"><input name="ConfiguracionesPregunta[' + Cont + '].ValorMin" id="ConfiguracionesPregunta_ValorMin-' + Cont + '" class="form-control" placeholder="Mínimo" type="number" value="" min="0" data-val="true" data-val-required="Este Campo es requerido"><span class="text-danger field-validation-valid" data-valmsg-for="ConfiguracionesPregunta[' + Cont + '].ValorMin" data-valmsg-replace="true"></span></div></div><div class="col-md-3"><div class="form-group"><input name="ConfiguracionesPregunta[' + Cont + '].ValorMax" id="ConfiguracionesPregunta_ValorMax-' + Cont + '" class="form-control" placeholder="Máximo" type="number" value="" min="0" data-val="true" data-val-required="Este Campo es requerido"><span class="text-danger field-validation-valid" data-valmsg-for="ConfiguracionesPregunta[' + Cont + '].ValorMax" data-valmsg-replace="true"></span></div></div><div class="col-md-3"><div class="form-group"><input name="ConfiguracionesPregunta[' + Cont + '].Ponderacion" id="ConfiguracionesPregunta_Ponderacion-' + Cont + '" class="form-control" placeholder="Ponderación" type="number" value="" min="0" data-val="true" data-val-required="Este Campo es requerido"><span class="text-danger field-validation-valid" data-valmsg-for="ConfiguracionesPregunta[' + Cont + '].Ponderacion" data-valmsg-replace="true"></span></div></div><div class="col-md-1 smart-form"><div class="form-group" style="margin-left:20px; margin-top:5px;"><label class="checkbox"><input name="ConfiguracionesPregunta[' + Cont + '].Sumatoria" id="ConfiguracionesPregunta_Sumatoria-' + Cont + '" value="true" type="checkbox"><i></i></label></div></div><div class="col-md-1"><div class="form-group" style="height:50px;"><button class="btn btn-danger form-control" href="" type="button" rel="tooltip" title="" data-placement="bottom" data-original-title="Eliminar Ponderación" onclick="EliminarPonderacion(' + Cont + ')"><i class="fa fa-trash-o" aria-hidden="true"></i></button></div></div></div></div>');

    return false;
}




function ConsultarConfiguracion() {
    var url = '/Configuraciones/ObtenerConfigOpciones/'
    $.ajax({
        url: url,
        method: "GET",
        dataType: "JSON",
        async: false,
        success: function (data) {
            console.log(data);
            if (data != null) {
                cantidad = data.cantidad;
            } else {
                cantidad = 0;
            }
        },
        error: function () {
            cantidad = 0;
        }
    });
}

function RedirigirConfiguracion() {
    swal({
        title: 'Sin Configuración',
        text: "Para la creación de preguntas y opciones es necesario guardar la configuración de Cantidad de Opciones",
        type: 'warning',
        showCancelButton: false,
        confirmButtonText: '¡Ir a la configuración!',
        confirmButtonClass: 'btn btn-primary',
        showLoaderOnConfirm: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
    }).then(function () {
        window.location.href = "/Configuraciones/ConfiguracionOpciones/";
    });
}

$(document).ready(function () {
    ConsultarConfiguracion();
    if (cantidad == 0) {
        RedirigirConfiguracion();
    }

    if ($('#TipoPregunta').val() == '2') {
        $('#ConfiguracionPregunta').removeClass('hidden');
    } else {
        $('#ConfiguracionPregunta').addClass('hidden');
    }

    $('#TipoPregunta').change(function () {
        var tipo = $(this).val();
        if (tipo == '2') {
            $('#ConfiguracionPregunta').removeClass('hidden');
        } else {
            $('#ConfiguracionPregunta').addClass('hidden');
        }
    });
});

function EliminarOpcion(opcion) {
    var o = $('#label-' + opcion).text().replace('* ', '');
    var id = $('#Opciones_Id-' + opcion).val();
    swal({
        title: '¿Desea eliminar la ' + o + '?',
        text: "Esta acción no se podrá revertir",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'red',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Si, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-primary',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                if (id != null && id != 0 && id != "" && id != undefined) {
                    var url = '/Opciones/Eliminar/';
                    $.ajax({
                        url: url + id,
                        method: "GET",
                        dataType: "json",
                        success: function (data) {
                            if (data.success) {
                                swal({
                                    type: 'success',
                                    title: '¡Opción eliminada!',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                }).then(function () {
                                    RemoverOpcion(opcion);
                                })
                            } else {
                                swal({
                                    title: "¡Algo salió mal!",
                                    text: "¡Ocurrió un error al eliminar la opción, si el problema continua comunicate con el administrador!",
                                    type: "error",
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                });
                            }
                        },
                        error: function () {
                            swal({
                                title: "¡Algo salió mal!",
                                text: "¡Ocurrió un error al eliminar la opción, si el problema continua comunicate con el administrador!",
                                type: "error",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            });
                        }
                    });
                } else {
                    swal({
                        type: 'success',
                        title: '¡Opción eliminada!',
                        allowOutsideClick: false,
                        confirmButtonColor: '#0c7cd5',
                        allowEscapeKey: false
                    }).then(function () {
                        RemoverOpcion(opcion);
                    })
                }
            })
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function RemoverOpcion(opcion) {
    $("#opciones-" + opcion).remove();
    ActualizarOpciones();
}

function ActualizarOpciones() {
    var Cont = $("#contenedorOpciones .row").length;
    var Consecutivo = 0;
    if (Cont > 0) {
        for (var i = 0; i <= Cont; i++) {
            var div = $('#opciones-' + i);
            if (div.length) {
                var inputId = $('#Opciones_Id-' + i);
                inputId.attr("name", "Opciones['" + Consecutivo + "'].Id");
                inputId.attr("id", "Opciones_Id-" + Consecutivo);
                var inputNombre = $('#Opciones_Nombre-' + i);
                inputNombre.attr("name", "Opciones['" + Consecutivo + "'].Nombre");
                inputNombre.attr("id", "Opciones_Nombre-" + Consecutivo);
                var inputValor = $('#Opciones_Valor-' + i);
                inputValor.attr("name", "Opciones['" + Consecutivo + "'].Valor");
                inputNombre.attr("id", "Opciones_Valor-" + Consecutivo);
                var boton = $('#opciones-' + i + ' button');
                boton.attr("onclick", "EliminarOpcion('" + Consecutivo + "')");
                div.attr("id", "opciones-" + Consecutivo);
                var label = $('#label-' + i);
                label.empty();
                Consecutivo++;
                label.append('<font color="red">* </font>Opción ' + Consecutivo);
                label.attr("id", "label-" + (Consecutivo - 1));
            }
        }
    }
}


function EliminarPonderacion(ponderacion) {
    var id = $('#ConfiguracionesPregunta_Id-' + ponderacion).val();
    swal({
        title: '¿Desea eliminar esta Ponderación?',
        text: "Esta acción no se podrá revertir",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'red',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Si, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-primary',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                if (id != null && id != 0 && id != "" && id != undefined) {
                    var url = '/Preguntas/EliminarPonderacion/';
                    $.ajax({
                        url: url + id,
                        method: "GET",
                        dataType: "json",
                        success: function (data) {
                            if (data.success) {
                                swal({
                                    type: 'success',
                                    title: '¡Ponderación eliminada!',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                }).then(function () {
                                    RemoverPonderacion(ponderacion);
                                })
                            } else {
                                swal({
                                    title: "¡Algo salió mal!",
                                    text: "¡Ocurrió un error al eliminar la ponderación, si el problema continua comunicate con el administrador!",
                                    type: "error",
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#0c7cd5',
                                    allowEscapeKey: false
                                });
                            }
                        },
                        error: function () {
                            swal({
                                title: "¡Algo salió mal!",
                                text: "¡Ocurrió un error al eliminar la ponderación, si el problema continua comunicate con el administrador!",
                                type: "error",
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            });
                        }
                    });
                } else {
                    swal({
                        type: 'success',
                        title: '¡Ponderación eliminada!',
                        allowOutsideClick: false,
                        confirmButtonColor: '#0c7cd5',
                        allowEscapeKey: false
                    }).then(function () {
                        RemoverPonderacion(ponderacion);
                    })
                }
            })
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function RemoverPonderacion(ponderacion) {
    $("#config-" + ponderacion).remove();
    ActualizarPonderaciones();
}

function ActualizarPonderaciones() {
    var Cont = $("#contenedorConfigs .row").length;
    var Consecutivo = 0;
    if (Cont > 0) {
        for (var i = 0; i <= Cont; i++) {
            var div = $('#config-' + i);
            if (div.length) {
                var inputId = $('#ConfiguracionesPregunta_Id-' + i);
                inputId.attr("name", "ConfiguracionesPregunta['" + Consecutivo + "'].Id");
                inputId.attr("id", "ConfiguracionesPregunta_Id-" + Consecutivo);
                var inputValorMin = $('#ConfiguracionesPregunta_ValorMin-' + i);
                inputValorMin.attr("name", "ConfiguracionesPregunta['" + Consecutivo + "'].ValorMin");
                inputValorMin.attr("id", "ConfiguracionesPregunta_ValorMin-" + Consecutivo);
                var inputValorMax = $('#ConfiguracionesPregunta_ValorMax-' + i);
                inputValorMax.attr("name", "ConfiguracionesPregunta['" + Consecutivo + "'].ValorMax");
                inputValorMax.attr("id", "ConfiguracionesPregunta_ValorMax-" + Consecutivo);
                var inputPonderacion = $('#ConfiguracionesPregunta_Ponderacion-' + i);
                inputPonderacion.attr("name", "ConfiguracionesPregunta['" + Consecutivo + "'].Ponderacion");
                inputPonderacion.attr("id", "ConfiguracionesPregunta_Ponderacion-" + Consecutivo);
                var inputSumatoria = $('#ConfiguracionesPregunta_Sumatoria-' + i);
                inputSumatoria.attr("name", "ConfiguracionesPregunta['" + Consecutivo + "'].Sumatoria");
                inputSumatoria.attr("id", "ConfiguracionesPregunta_Sumatoria-" + Consecutivo);
                var boton = $('#config-' + i + ' button');
                boton.attr("onclick", "EliminarPonderacion('" + Consecutivo + "')");
                div.attr("id", "config-" + Consecutivo);
                Consecutivo++;
            }
        }
    }
}