﻿$(document).ready(function () {

    $("#logoActual").width(200);
    $("#logoActual").removeClass("hidden");


});
$("#InputFile").fileinput({
    showUpload: false,
    browseLabel: "Seleccionar Archivo",
    removeLabel: "Eliminar",
    allowedFileExtensions: ["png", "jpg", "jpeg"],
    maxFileCount: 1,
});
$("#archivos").fileinput({
    showUpload: false,
    browseLabel: "Seleccionar Archivos",
    removeLabel: "Eliminar",
    allowedFileExtensions: ["png", "jpg", "jpeg"],
    maxFileCount: 1,
});