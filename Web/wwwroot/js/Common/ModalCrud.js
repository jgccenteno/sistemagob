﻿function CrearEditar(Controlador, Metodo) {
    if ($("form").valid()) {
        $.ajax({
            data: $("form").serialize(),
            url: '/' + Controlador + '/' + Metodo,
            type: 'POST',
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            success: function (result) {

                if (result.success) {
                    swal({
                        title: "¡Correcto!",
                        text: result.msj,
                        type: "success"
                    }).then(function () {
                        location.reload();

                    });
                } else {
                    swal({
                        title: "¡Error!",
                        text: result.msj,
                        type: "error"
                    }).then(function () {
                        location.reload();

                    });
                }

            }
        });
    }
    return false;
};