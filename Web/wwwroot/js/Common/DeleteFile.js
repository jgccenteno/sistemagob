﻿function eliminar(url, id, nombre) {
    swal({
        title: '¿Desea eliminar la imágen: ' + nombre + '?',
        text: "Esta acción no se podrá revertir",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'red',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Si, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-primary',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    url: url + id,
                    method: "GET",
                    dataType: "html",
                    success: function (data) {

                        swal({
                            type: 'success',
                            title: '¡Registro eliminado!',
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }).then(function () {
                            location.reload();
                        })
                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Verifica que los datos sean correctos, si el problema continua comunicate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.reload();
                        });
                    }
                });
            })
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    })
};

