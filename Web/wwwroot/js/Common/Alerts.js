﻿

function eliminarCSRF(url, id, nombre) {
    swal({
        title: '¿Desea eliminar el registro: ' + nombre + '?',
        text: "Esta acción no se podrá revertir",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'red',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Sí, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var form = $('#__AjaxAntiForgeryForm');
                var token = $('input[name="__RequestVerificationToken"]', form).val();
                $.ajax({
                    url: url + id,
                    method: "post",
                    dataType: "json",
                    data: {
                        __RequestVerificationToken: token,
                        id: id
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({
                                type: 'success',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        } else {
                            swal({
                                type: 'info',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        }

                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Ha ocurrido un error al eliminar el registro, si el problema continua comunicate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.reload();
                        });
                    }
                });
            });
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    });
};



function eliminarCSRFEdit(url, id, nombre, c) {
    swal({
        title: '¿Desea eliminar el registro: ' + nombre + '?',
        text: "Esta acción no se podrá revertir",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'red',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Sí, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var form = $('#__AjaxAntiForgeryForm');
                var token = $('input[name="__RequestVerificationToken"]', form).val();
                $.ajax({
                    url: url + id,
                    method: "post",
                    dataType: "json",
                    data: {
                        __RequestVerificationToken: token,
                        id: id
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({
                                type: 'success',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.href = '/' + c + '/Index';
                            });
                        } else {
                            swal({
                                type: 'info',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.href = '/' + c + '/Index';
                            });
                        }

                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Ha ocurrido un error al eliminar el registro, si el problema continua comunicate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.href = '/' + c + '/Index';
                        });
                    }
                });
            });
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    })
};


function agregarLista(id, nombre) {
    swal({
        title: '¿Desea añadir a su lista el expediente de ' + nombre + '?',
        text: "Esta acción no se podrá revertir",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'red',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Sí, agregar!',
        cancelButtonText: '¡No, cancelar!',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var form = $('#__AjaxAntiForgeryForm');
                var token = $('input[name="__RequestVerificationToken"]', form).val();
                $.ajax({
                    url: "/Expedientes/AgregarResponsable/?id=" + id,
                    method: "post",
                    dataType: "json",
                    data: {
                        __RequestVerificationToken: token,
                        id: id
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({
                                type: 'success',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        } else {
                            swal({
                                type: 'info',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.reload();
                            });
                        }

                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Ha ocurrido un error al añadir el expediente, si el problema continua comunicate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.reload();
                        });
                    }
                });
            });
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    });
};

function EditarVigencia() {

    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    var id = $('#Id').val();

    var vigencia = $('#Vigencia').val();

    console.log("La vigencia es: " + vigencia);
    console.log("token: " + token);

    $.ajax({
        url: "/Expedientes/EditarVigencia/?id=" + id + "&fecha=" + vigencia,
        method: "post",
        dataType: "json",
        data: {
            __RequestVerificationToken: token,
            id: id,
            fecha: vigencia
        },
        success: function (data) {
            if (data.success) {
                swal({
                    type: 'success',
                    title: data.message,
                    allowOutsideClick: false,
                    confirmButtonColor: '#0c7cd5',
                    allowEscapeKey: false
                }).then(function () {
                    location.reload();
                });
            } else {
                swal({
                    type: 'info',
                    title: data.message,
                    allowOutsideClick: false,
                    confirmButtonColor: '#0c7cd5',
                    allowEscapeKey: false
                }).then(function () {
                    location.reload();
                });
            }

        },
        error: function () {
            swal({
                title: "¡Algo salió mal!",
                text: "¡Ha ocurrido un error al añadir el expediente, si el problema continua comunicate con el administrador!",
                type: "error",
                allowOutsideClick: false,
                confirmButtonColor: '#0c7cd5',
                allowEscapeKey: false
            }, function () {
                location.reload();
            });
        }
    });
}
