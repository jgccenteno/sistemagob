﻿$(document).ready(function () {
    $('#idProfesores').select2({
        placeholder: "SELECCIONAR....",
        allowClear: true,
        selectOnClose: false,
        language: {
            noResults: function (params) {
                return "LA INSTITUCIÓN NO CUENTA CON PROFESORES";
            }
        }
    });
 
});

$("#IdInstitucion").on('change', function () {
    if ($("#IdInstitucion").val() !== "") {
        $('#idProfesores')
            .find('option')
            .remove()
            .end();
        var idInstituto = $("#IdInstitucion").val();
        url = '/Grupos/ObtenerProfesoresPorInstitucion?id=' + idInstituto;
        datos = {};
        FillProfesoresCombo(url);
    }
});

function FillProfesoresCombo(url) {

    $.ajax({
        url: url,
        method: "GET",
        dataType: 'json',
        contentType: 'application/json',
        processData: false,
        success: function (data) {
            console.log(data);
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    //console.log(data[i].id + " - " + data[i].nombre)
                    $("#idProfesores").append("<option value=" + data[i].id + ">" + data[i].nombreCompleto + "</option>");
                }
                $('#idProfesores').attr("placeholder", "Seleccione Profesores");
            } else {
                swal({
                    title: "Importante",
                    text: "La institución seleccionada no cuenta con profesores",
                    type: "warning",
                    allowOutsideClick: false,
                    confirmButtonColor: '#0c7cd5',
                    allowEscapeKey: false
                });
            }
           
        },
        error: function () {
            swal({
                title: "¡Algo salió mal!",
                text: "Error al cargar los profesores, comuniquese con el administrador",
                type: "error",
                allowOutsideClick: false,
                confirmButtonColor: '#0c7cd5',
                allowEscapeKey: false
            });
        }
    });
}