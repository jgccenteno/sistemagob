﻿
var fecha = new Date();
fecha.setDate(fecha.getDate());
var minDate = fecha;

$("#FechaInicio").datepicker({
    autoclose: true,
    startDate: minDate
}).on('changeDate', function (selected) {
    minDate = new Date(selected.date.valueOf());
    $('#FechaFin').datepicker('setStartDate', minDate);
});
$("#FechaFin").datepicker({
    autoclose: true
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#FechaInicio').datepicker('setEndDate', maxDate);
});

$("#FechaUno").datepicker({
    autoclose: true,
    endDate: fecha, 
}).on('changeDate', function (selected) {
    minDate = new Date(selected.date.valueOf());
    $('#FechaDos').datepicker('setStartDate', minDate);
});
$("#FechaDos").datepicker({
    autoclose: true,
    startDate: minDate
}).on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#FechaUno').datepicker('setEndDate', maxDate);
});


