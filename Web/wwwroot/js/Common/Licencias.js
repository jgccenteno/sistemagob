﻿

﻿function SolicitarL(url, id, idPersona) {
     if (idPersona > 0) {
         swal({
             title: '¿Esta seguro de iniciar el proceso de Solicitud de la Licencia de Funcionamiento?',
             text: "Esta acción no se podrá revertir.",
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#0c7cd5',
             cancelButtonColor: 'Red',
             confirmButtonText: '¡Sí, solicitar!',
             cancelButtonText: '¡No, cancelar!',
             showLoaderOnConfirm: true,
             preConfirm: function () {
                 return new Promise(function (resolve, reject) {
                     var form = $('#__AjaxAntiForgeryForm');
                     var token = $('input[name="__RequestVerificationToken"]', form).val();
                     $.ajax({
                         url: url + id,
                         method: "post",
                         dataType: "json",
                         data: {
                             __RequestVerificationToken: token,
                             id: id
                         },
                         success: function (data) {
                             if (data.success) {
                                 swal({
                                     type: 'success',
                                     title: data.message,
                                     allowOutsideClick: false,
                                     confirmButtonColor: '#0c7cd5',
                                     allowEscapeKey: false
                                 }).then(function () {
                                     location.href = "/LicenciasFuncionamiento/Requisitos/" + data.data;

                                 });
                             } else {
                                 swal({
                                     type: 'info',
                                     title: data.message,
                                     allowOutsideClick: false,
                                     confirmButtonColor: '#0c7cd5',
                                     allowEscapeKey: false
                                 }).then(function () {
                                     location.reload();
                                 });
                             }

                         },
                         error: function () {
                             swal({
                                 title: "¡Algo salió mal!",
                                 text: "¡Ha ocurrido un error al eliminar el registro, si el problema continua comunicate con el administrador!",
                                 type: "error",
                                 allowOutsideClick: false,
                                 confirmButtonColor: '#0c7cd5',
                                 allowEscapeKey: false
                             }, function () {
                                 location.reload();
                             });
                         }
                     });
                 });
             },
             allowOutsideClick: false,
             allowEscapeKey: false
         });
     } else {
         swal({
             title: '¡Asigne un representante antes de solicitar la licencia de funcionamiento!',
             text: 'Este establecimiento no tiene una representante asignado.',
             type: "error",
             animation: false,
             customClass: 'animated tada',
             allowOutsideClick: false,
             confirmButtonColor: '#0c7cd5',
             allowEscapeKey: false



         });
     }
    
};



﻿function eliminarCSRFEdit(url, id, nombre,c) {
    swal({
        title: '¿Desea eliminar el registro: ' + nombre + '?',
        text: "Esta acción no se podrá revertir",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: 'red',
        cancelButtonColor: '#0c7cd5',
        confirmButtonText: '¡Sí, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var form = $('#__AjaxAntiForgeryForm');
                var token = $('input[name="__RequestVerificationToken"]', form).val();
                $.ajax({
                    url: url + id,
                    method: "post",
                    dataType: "json",
                    data: {
                        __RequestVerificationToken: token,
                        id: id
                    },
                    success: function (data) {
                        if (data.success) {
                            swal({
                                type: 'success',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.href = '/' + c + '/Index';
                            });
                        } else {
                            swal({
                                type: 'info',
                                title: data.message,
                                allowOutsideClick: false,
                                confirmButtonColor: '#0c7cd5',
                                allowEscapeKey: false
                            }).then(function () {
                                location.href = '/' + c + '/Index';
                            });
                        }

                    },
                    error: function () {
                        swal({
                            title: "¡Algo salió mal!",
                            text: "¡Ha ocurrido un error al eliminar el registro, si el problema continua comunicate con el administrador!",
                            type: "error",
                            allowOutsideClick: false,
                            confirmButtonColor: '#0c7cd5',
                            allowEscapeKey: false
                        }, function () {
                            location.href = '/' + c + '/Index';
                        });
                    }
                });
            });
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    })
};


