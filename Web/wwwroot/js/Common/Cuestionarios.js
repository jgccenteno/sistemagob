﻿var fecha = new Date();
fecha.setDate(fecha.getDate());
var minDate = fecha;

$("#FechaPublicacion").datepicker({
    autoclose: true,
    startDate: minDate
}).on('changeDate', function (selected) {
    minDate = new Date(selected.date.valueOf());
    $('#FechaExpiracion').datepicker('setStartDate', minDate);
});

$("#FechaExpiracion").datepicker({
    autoclose: true,
    startDate: minDate
}).on('changeDate', function (selected) {
    minDate = new Date(selected.date.valueOf());
    $('#FechaPublicacion').datepicker('setEndDate', minDate);
});


var FechaN = new Date();
var anios = FechaN.getFullYear();

FechaN.setFullYear(anios - 4);

$("#FechaNacimiento").datepicker({
    autoclose: true,
    format: "dd/mm/yyyy",
    startView: 2,
    language: 'pt-BR',
    endDate: FechaN

});

