﻿$('#ModalMaster').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var url = button.attr("href");
    var modal = $(this);
    modal.find('.modal-content').load(url);
});

$(function () {
    $('#ModalMaster').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
        $('#modal-container .modal-content').empty();
        console.log("Modal Clear")
    });
});

$('#ModalPDF').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var url = button.attr("href");
    var modal = $(this);
    modal.find('.modal-content').load(url);
});

$(function () {
    $('#ModalPDF').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
        $('#modal-container .modal-content').empty();
        console.log("Modal Clear")
    });
});